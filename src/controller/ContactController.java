package controller;

import dao.ContactDAO;
import dao.GroupDAO;
import entity.Contact;
import entity.Group;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.function.Predicate;


public class ContactController {


    @FXML
    BorderPane mainPanel;

    @FXML
    public ComboBox<Group> cbGroup;

    @FXML
    Button btnDelete;

    @FXML
    TextField searchField;

    @FXML
    private TableView<Contact> contactTableView;
    
    private ContactDAO contactData;
    private GroupDAO groupData;
    private FilteredList<Contact> filteredList;

    private Predicate<Contact> wantAllContact;

    private ObservableList<Contact> contactList;

    private ObservableList<Group> groupList;


    public void initialize() {
        contactData = ContactDAO.getInstance();
        groupData = new GroupDAO();
        // Load contact data
        contactData.loadContact();
        groupData.loadGroup();

        // Load data to group list
        groupList = groupData.getGroupItem();

        // Initialize dropdown ComboBox
        refreshAndShowGroup();

        // Create filter condition for ALL
        wantAllContact = new Predicate<Contact>() {
            @Override
            public boolean test(Contact contact) {
                return true;
            }
        };

        // Load data to contact list
        contactList = contactData.getContact();
        filteredList = new FilteredList<>(contactList, wantAllContact);


        // Populate data to Table View
        contactTableView.setItems(filteredList);
        contactTableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        // Add Event Handler for Delete Button
        btnDelete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Contact selectedContact = contactTableView.getSelectionModel().getSelectedItem();
                if (selectedContact == null) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("No Contact Selected");
                    alert.setHeaderText(null);
                    alert.setContentText("Please select the contact you want to delete");
                    alert.showAndWait();
                } else {
                    deleteContact(selectedContact);
                }
            }
        });

    }


    // Add a contact
    @FXML
    public void handleAddContact() {
        // Create a dialog
        Dialog<ButtonType> addDialog = new Dialog<>();
        addDialog.initOwner(mainPanel.getScene().getWindow());
        addDialog.setTitle("Add New Contact");

        // Load the dialog when it is activated
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/ui/addContact.fxml"));

        // Set scene for the dialog
        try {
            addDialog.getDialogPane().setContent(fxmlLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        // Add result buttons for the dialog
        addDialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        addDialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);

        // Show the dialog and check a result of the action
        Optional<ButtonType> addResult = addDialog.showAndWait();
        if (addResult.isPresent() && addResult.get() == ButtonType.OK) {
            // Get Controller, then return current input of contact
            AddContactController addContactController = fxmlLoader.getController();
            Contact currContactView = addContactController.getCurrContactView();

            // Create an alert
            Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);
            infoAlert.setTitle("Information");
            // Checking validity of the input
            if (currContactView == null) {
                Alert validAlert = new Alert(Alert.AlertType.WARNING);
                validAlert.setTitle("Invalid Contact");
                validAlert.setContentText("You must fill all required field");
                validAlert.showAndWait();
            }
            else if (contactList.contains(currContactView)) {
                infoAlert.setContentText("Information of contact is existed");
                infoAlert.showAndWait();
            } else {
                infoAlert.setContentText("Contact has been added");
                infoAlert.showAndWait();

                // If the input valid, add contact to list
                ContactDAO.getInstance().addContactItem(currContactView);
                // Select the latest one
                contactTableView.getSelectionModel().select(currContactView);
                // Then, save it
                contactData.saveToFile();
            }

        }
    }


    // Update a contact
    @FXML
    public void handleUpdateContact() {
        // Checking currently selected contact for updating
        Contact selectedContact = contactTableView.getSelectionModel().getSelectedItem();
        if (selectedContact == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("No Contact Selected");
            alert.setHeaderText(null);
            alert.setContentText("Please select the contact you want to update");
            alert.showAndWait();
            return;
        }

        // If contact was selected, then create dialog
        Dialog<ButtonType> updateDialog = new Dialog<>();
        updateDialog.initOwner(mainPanel.getScene().getWindow());
        updateDialog.setTitle("Update a Contact");

        // Load the dialog when it is activated
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/ui/updateContact.fxml"));

        // Set scene for the dialog
        try {
            updateDialog.getDialogPane().setContent(fxmlLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        // Add result buttons for the dialog
        updateDialog.getDialogPane().getButtonTypes().add(ButtonType.APPLY);
        updateDialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);

        // Get information for selected contact to show on Update Dialog
        UpdateContactController updateContactController = fxmlLoader.getController();
        updateContactController.editContactView(selectedContact);

        // Show the dialog and check a result of the action
        Optional<ButtonType> updateResult = updateDialog.showAndWait();
        if (updateResult.isPresent() && updateResult.get() == ButtonType.APPLY) {
            // Return current input of contact
            Contact currContactView = updateContactController.getCurrContactView();

            // Create an alert
            Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);
            infoAlert.setTitle("Information");
            // Checking if information of the contact has changed
            if (currContactView == null) {
                Alert validAlert = new Alert(Alert.AlertType.WARNING);
                validAlert.setTitle("Invalid Contact");
                validAlert.setContentText("You must fill all required field");
                validAlert.showAndWait();
            } else if (contactList.contains(currContactView)) {
                infoAlert.setContentText("Information of contact is existed");
                infoAlert.showAndWait();
            } else {
                infoAlert.setContentText("Contact has been updated");
                infoAlert.showAndWait();

                // If the input valid, change current contact
                updateContactController.updateContact(selectedContact);
                // Then, save it
                contactData.saveToFile();
            }
        }
    }

    // Delete a selected contact
    public void deleteContact(Contact contact) {
        // Create an alert
        Alert deleteAlert = new Alert(Alert.AlertType.CONFIRMATION);
        deleteAlert.setTitle("Delete Contact");
        deleteAlert.setContentText("Do you want to delete selected contact?");

        // Show the alert and check the result
        Optional<ButtonType> deleteResult = deleteAlert.showAndWait();
        if (deleteResult.isPresent() && deleteResult.get() == ButtonType.OK) {
            contactData.deleteContactItem(contact);
            contactData.saveToFile();
        }
    }

    // Search a contact
    @FXML
    public void handleFilterContact() {
        String keyWord = searchField.getText().trim();
        // Filter for ALL Group
        if (cbGroup.getSelectionModel().getSelectedItem().equals(new Group("All"))
            && keyWord.isEmpty()) {
            filteredList.setPredicate(wantAllContact);
        } else if (cbGroup.getSelectionModel().getSelectedItem().equals(new Group("All"))) {
            // Create filter for specific Keyword
            Predicate<Contact> wantKeyword = new Predicate<Contact>() {
                @Override
                public boolean test(Contact contact) {
                    String group = cbGroup.getSelectionModel().getSelectedItem().getName();

                    DateTimeFormatter formatter = contactData.formatter;

                    return (contact.getFirstName().toLowerCase().contains(keyWord.toLowerCase())
                            || contact.getLastName().toLowerCase().contains(keyWord.toLowerCase())
                            || contact.getPhone().contains(keyWord)
                            || contact.getEmail().toLowerCase().contains(keyWord.toLowerCase())
                            || contact.getDob().format(formatter).contains(keyWord));
                }
            };

            filteredList.setPredicate(wantKeyword);
        } else {
            // Create filter for specific Group and Keyword
            Predicate<Contact> wantSearchContact = new Predicate<Contact>() {
                @Override
                public boolean test(Contact contact) {
                    String group = cbGroup.getSelectionModel().getSelectedItem().getName();

                    DateTimeFormatter formatter = contactData.formatter;

                    return (contact.getFirstName().toLowerCase().contains(keyWord.toLowerCase())
                            || contact.getLastName().toLowerCase().contains(keyWord.toLowerCase())
                            || contact.getPhone().contains(keyWord)
                            || contact.getEmail().toLowerCase().contains(keyWord.toLowerCase())
                            || contact.getDob().format(formatter).contains(keyWord))
                            && (contact.getGroup().equals(group));
                }
            };

            filteredList.setPredicate(wantSearchContact);
        }
    }


    // Manage group contact
    @FXML
    public void handleGroupContact() {
        // Create a dialog
        Dialog<ButtonType> groupDialog = new Dialog<>();
        groupDialog.initOwner(mainPanel.getScene().getWindow());
        groupDialog.setTitle("Manage Groups");

        // Load the dialog when it is activated
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/ui/group.fxml"));

        // Set scene for the dialog
        try {
            groupDialog.getDialogPane().setContent(fxmlLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        // Add result buttons for the dialog
        groupDialog.getDialogPane().getButtonTypes().add(ButtonType.APPLY);
        groupDialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);

        // Show the dialog and check the result
        Optional<ButtonType> groupResult = groupDialog.showAndWait();
        if (groupResult.isPresent() && groupResult.get() == ButtonType.APPLY) {
            // Get Group Controller, then get group list was modified
            GroupController groupController = fxmlLoader.getController();
            groupList = groupController.getTempGroupList();
            // Again, change ComboBox
            refreshAndShowGroup();

            // Save it to file
            groupController.saveGroupData();

        }
    }

    // Output all groups to dropdown list
    public void refreshAndShowGroup() {
        // Remove all options
        cbGroup.getItems().removeAll(cbGroup.getItems());
        // Add an ALL option and select it
        cbGroup.getItems().add(new Group("All"));
        cbGroup.getSelectionModel().selectFirst();
        // Add options to dropdown
        for (Group item : groupList) {
            cbGroup.getItems().add(item);
        }

    }

}
