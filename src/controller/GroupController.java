package controller;


import dao.ContactDAO;
import dao.GroupDAO;
import entity.Contact;
import entity.Group;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;


public class GroupController {

    @FXML
    TextField groupNameField;

    @FXML
    TextField searchField;

    @FXML
    ListView<Group> groupListView;

    private GroupDAO groupData;

    private ObservableList<Group> tempGroupList;
    
    public void initialize() {
        groupData = new GroupDAO();
        groupData.loadGroup();
        // Load data to group list
        tempGroupList = groupData.getGroupItem();


        // Set items for list view
        groupListView.setItems(tempGroupList);
        groupListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        // Add Change Listener for List View when it is selected
        groupListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Group>() {
            @Override
            public void changed(ObservableValue<? extends Group> observableValue, Group oldGroup, Group newGroup) {
                if (newGroup != null) {
                    Group item = groupListView.getSelectionModel().getSelectedItem();
                    groupNameField.setText(item.getName());
                }
            }
        });

    }

    // Add new group action
    @FXML
    public void handleAddGroup() {
        Group currGroup = getCurrGroupView();
        // Check add field is not empty
        if (!groupNameField.getText().trim().isEmpty()) {
            // Create an alert
            Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);
            infoAlert.setTitle("Information");
            if (tempGroupList.contains(currGroup)) {
                infoAlert.setContentText("Information of group is existed");
                infoAlert.showAndWait();
            } else {

                // Add new group to list
                Group newGroup = new Group(groupNameField.getText().trim());


                groupData.addGroupItem(newGroup);

            }
        } else {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setTitle("ERROR");
            errorAlert.setContentText("This field cannot empty!");
            errorAlert.show();
        }

    }

    // Update a group name
    @FXML
    public void handleUpdateGroup() {
        // Checking currently selected group for updating
        Group currGroup = getCurrGroupView();
        if (currGroup == null) {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setTitle("ERROR");
            errorAlert.setContentText("This field cannot empty!");
            errorAlert.show();
        } else {

            if (tempGroupList.contains(currGroup)) {
                Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);
                infoAlert.setTitle("Information");
                infoAlert.setContentText("Information of group is existed");
                infoAlert.showAndWait();
            } else {
                // Create an alert to confirm the change
                Alert confirmAlert = new Alert(Alert.AlertType.CONFIRMATION);
                confirmAlert.setTitle("Update Group");
                confirmAlert.setHeaderText("If you update this group, contacts in the group will be updated immediately");
                confirmAlert.setContentText("Are you still want to update this group?");

                Optional<ButtonType> deleteResult =  confirmAlert.showAndWait();
                if (deleteResult.isPresent() && deleteResult.get() == ButtonType.OK) {

                    // Update contacts in the chosen group
                    Iterator<Contact> inContactList = ContactDAO.getInstance().getContact().iterator();
                    while (inContactList.hasNext()) {
                        Contact item = inContactList.next();
                        if (item.getGroup().equals(groupListView.getSelectionModel().getSelectedItem().getName())) {
                            item.setGroup(groupNameField.getText().trim());
                        }
                    }

                    // Then, save changed contacts to file
                    ContactDAO.getInstance().saveToFile();


                    // Update List View
                    updateGroup(groupListView.getSelectionModel().getSelectedItem());
                }
            }
        }
    }

    // Return current input of Group
    private Group getCurrGroupView() {
        // Check update field is not empty
        if (!groupNameField.getText().trim().isEmpty()) {
            return new Group(groupNameField.getText().trim());
        } else {
            return null;
        }
    }

    // Set attr
    public void updateGroup(Group group) {
        group.setName(groupNameField.getText().trim());
    }


    // Delete a group
    @FXML
    public void handleDeleteGroup() {
        Group currGroup = getCurrGroupView();
        // Create an alert to confirm the change
        Alert confirmAlert = new Alert(Alert.AlertType.CONFIRMATION);
        confirmAlert.setTitle("Delete Group");
        confirmAlert.setHeaderText("If you delete this group, contacts in the group will be deleted immediately");
        confirmAlert.setContentText("Are you still want to delete this group?");

        if (currGroup == null) {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setTitle("ERROR");
            errorAlert.setContentText("This field cannot empty!");
            errorAlert.show();
        } else {
            Optional<ButtonType> deleteResult =  confirmAlert.showAndWait();
            if (deleteResult.isPresent() && deleteResult.get() == ButtonType.OK) {

                // Delete contacts in the chosen group
                try {
                    ObservableList<Contact> inContactList = ContactDAO.getInstance().getContact();
                    inContactList.removeIf(item -> item.getGroup().equals(groupListView.getSelectionModel().getSelectedItem().getName()));

                    groupData.deleteGroupItem(groupListView.getSelectionModel().getSelectedItem());

                    // Then, save changed contacts to file
                    ContactDAO.getInstance().saveToFile();
                } catch (ConcurrentModificationException e) {
                    e.printStackTrace();
                }

            }
        }

    }


    // Search a group
    @FXML
    public void handleFilterGroup() {
        String keyWord = searchField.getText().trim();
        Predicate<Group> wantFilterGroup = new Predicate<Group>() {
            @Override
            public boolean test(Group group) {
                return group.getName().toLowerCase().contains(keyWord.toLowerCase());
            }
        };

        FilteredList<Group> filteredList = new FilteredList<>(groupData.getGroupItem(), wantFilterGroup);
        groupListView.setItems(filteredList);
    }

    // Save data to file in GroupDAO
    public void saveGroupData() {
        groupData.saveGroupToFile();
    }

    //  Get the current group list for display purpose
    public ObservableList<Group> getTempGroupList() {
        return tempGroupList;
    }
}
