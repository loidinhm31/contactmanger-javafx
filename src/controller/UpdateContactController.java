package controller;

import dao.GroupDAO;
import entity.Contact;
import entity.Group;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.time.LocalDate;


public class UpdateContactController {
    @FXML
    TextField firstNameField;

    @FXML
    Label labelFirstName;

    @FXML
    TextField lastNameField;

    @FXML
    Label labelLastName;

    @FXML
    TextField phoneField;

    @FXML
    Label labelPhone;

    @FXML
    TextField emailField;

    @FXML
    Label labelEmail;

    @FXML
    DatePicker dobPicker;

    @FXML
    Label labelDob;

    @FXML
    ComboBox<Group> cbGroup;

    private String inputFirstName, inputLastName, inputPhone, inputEmail, inputGroup;
    private LocalDate inputDob;

    // Checking required status for Apply Contact
    private boolean isFirstNameFill;
    private boolean isLastNameFill;
    private boolean isPhoneFill;

    public void initialize() {
        // Add value for ComboBox
        GroupDAO groupDAO = new GroupDAO();
        groupDAO.loadGroup();
        ObservableList<Group> groupList = groupDAO.getGroupItem();
        cbGroup.setItems(groupList);
    }


    public void editContactView(Contact contact) {
        // Set fields for current contact view
        firstNameField.setText(contact.getFirstName());
        lastNameField.setText(contact.getLastName());
        phoneField.setText(contact.getPhone());
        emailField.setText(contact.getEmail());
        dobPicker.setValue(contact.getDob());
        cbGroup.getSelectionModel().select(new Group(contact.getGroup()));

        // Initialize value for getting current Contact View
        inputFirstName = firstNameField.getText().trim();
        inputLastName = lastNameField.getText();
        inputPhone = phoneField.getText();
        inputEmail = emailField.getText();
        inputDob = dobPicker.getValue();
        inputGroup = cbGroup.getSelectionModel().getSelectedItem().getName();
    }

    // Set attrs for current contact
    public void updateContact(Contact contact){
        contact.setFirstName(firstNameField.getText().trim());
        contact.setLastName(lastNameField.getText().trim());
        contact.setPhone(phoneField.getText().trim());
        contact.setEmail(emailField.getText().trim());
        contact.setDob(dobPicker.getValue());
        contact.setGroup(cbGroup.getSelectionModel().getSelectedItem().getName());
    }

    // Return current input of contact
    public Contact getCurrContactView() {
        if (handleCheckInput()) {
            Contact currContact = new Contact(inputFirstName,
                    inputLastName,
                    inputPhone,
                    inputEmail,
                    inputDob,
                    inputGroup);
            return currContact;
        } else {
            return null;
        }

    }

    @FXML
    public boolean handleCheckInput() {
        String firstName = firstNameField.getText().trim();
        String lastName = lastNameField.getText().trim();
        String phoneNumber = phoneField.getText().trim();
        String email = emailField.getText().trim();
        LocalDate dob = dobPicker.getValue();


        // Check first name
        if (firstName.isEmpty()) {
            labelFirstName.setVisible(true);

            isFirstNameFill = false;
        } else {
            labelFirstName.setVisible(false);
            inputFirstName = firstName;

            isFirstNameFill = true;

        }

        // Check last name
        if (lastName.isEmpty()) {
            labelLastName.setVisible(true);

            isLastNameFill = false;
        } else {
            labelLastName.setVisible(false);
            inputLastName = lastName;

            isLastNameFill = true;
        }

        // Check phone number
        if (phoneNumber.isEmpty() || !phoneNumber.matches("\\d+")) {
            labelPhone.setVisible(true);

            isPhoneFill = false;
        } else {
            labelPhone.setVisible(false);
            inputPhone = phoneNumber;

            isPhoneFill = true;
        }

        // Check email
        String emailNamePtrn = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (email.isEmpty() || !email.trim().matches(emailNamePtrn)) {
            labelEmail.setVisible(true);
        } else {
            labelEmail.setVisible(false);
            inputEmail = email;
        }

        // Check birth date
        if (dob.isAfter(LocalDate.now())) {
            labelDob.setVisible(true);
        } else {
            labelDob.setVisible(false);
            inputDob = dob;
        }

        inputGroup = cbGroup.getSelectionModel().getSelectedItem().getName();

        // Set Condition to get New Contact
        return isFirstNameFill && isLastNameFill && isPhoneFill;
    }

}
