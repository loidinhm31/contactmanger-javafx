package controller;

import dao.GroupDAO;
import entity.Contact;
import entity.Group;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.time.LocalDate;

public class AddContactController {
    @FXML
    TextField firstNameField;

    @FXML
    Label labelFirstName;

    @FXML
    TextField lastNameField;

    @FXML
    Label labelLastName;

    @FXML
    TextField phoneNumberField;

    @FXML
    Label labelPhone;

    @FXML
    TextField emailField;

    @FXML
    Label labelEmail;

    @FXML
    DatePicker dobPicker;

    @FXML
    Label labelDob;

    @FXML
    ComboBox<Group> cbGroup;


    private String inputFirstName, inputLastName, inputPhone, inputEmail, inputGroup;
    private LocalDate inputDob;

    // Instance field for checking required status to Apply Contact
    private boolean isFirstNameFill;
    private boolean isLastNameFill;
    private boolean isPhoneFill;

    public void initialize() {
        // Create default value for non-required fields
        inputEmail = "None";
        inputDob = LocalDate.now();
        inputGroup = "None";

        // Add value for ComboBox
        GroupDAO groupDAO = new GroupDAO();
        groupDAO.loadGroup();
        ObservableList<Group> groupList = groupDAO.getGroupItem();
        cbGroup.setItems(groupList);
    }

    // Return current input of contact
    public Contact getCurrContactView() {
        if (handleCheckInput()) {
            Contact currContact = new Contact(inputFirstName,
                                        inputLastName,
                                        inputPhone,
                                        inputEmail,
                                        inputDob,
                                        inputGroup);
            return currContact;
        } else {
            return null;
        }
    }

    // Check valid input
    @FXML
    public boolean handleCheckInput() {
        String firstName = firstNameField.getText().trim();
        String lastName = lastNameField.getText().trim();
        String phoneNumber = phoneNumberField.getText().trim();
        String email = emailField.getText().trim();
        LocalDate dob = dobPicker.getValue();

        // Checking when typing
        if (firstNameField.isFocused()) {
            // Check first name
            if (firstName.isEmpty()) {
                labelFirstName.setVisible(true);

                isFirstNameFill = false;
            } else {
                labelFirstName.setVisible(false);
                inputFirstName = firstName;

                isFirstNameFill = true;
            }
        } else if (lastNameField.isFocused()) {
            // Check last name
            if (lastName.isEmpty()) {
                labelLastName.setVisible(true);

                isLastNameFill = false;
            } else {
                labelLastName.setVisible(false);
                inputLastName = lastName;

                isLastNameFill = true;
            }
        } else if (phoneNumberField.isFocused()) {
            // Check phone number
            if (phoneNumber.isEmpty() || !phoneNumber.matches("\\d+")) {
                labelPhone.setVisible(true);

                isPhoneFill = false;
            } else {
                labelPhone.setVisible(false);
                inputPhone = phoneNumber;

                isPhoneFill = true;
            }
        } else if (emailField.isFocused()) {
            // Check email
            String emailNamePtrn = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            if (email.isEmpty() || !email.trim().matches(emailNamePtrn)) {
                labelEmail.setVisible(true);
            } else {
                labelEmail.setVisible(false);
                inputEmail = email;
            }
        } else if (dobPicker.isFocused()) {
            // Check birth date
            if (dob.isAfter(LocalDate.now())) {
                labelDob.setVisible(true);
            } else {
                labelDob.setVisible(false);
                inputDob = dob;
            }
        } else if (cbGroup.isFocused()) {
            inputGroup = cbGroup.getSelectionModel().getSelectedItem().getName();
        }

        // Set Condition to get New Contact
        return isFirstNameFill && isLastNameFill && isPhoneFill;
    }
}
