package entity;


import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.time.LocalDate;

public class Contact {

    private SimpleStringProperty firstName = new SimpleStringProperty("");
    private SimpleStringProperty lastName = new SimpleStringProperty("");
    private SimpleStringProperty phone = new SimpleStringProperty("");
    private SimpleStringProperty email = new SimpleStringProperty("");
    private SimpleObjectProperty dob;
    private SimpleStringProperty group = new SimpleStringProperty("");

    public Contact(String firstName, String lastName, String phone, String email, LocalDate dob, String group) {
        this.firstName.set(firstName);
        this.lastName.set(lastName);
        this.phone.set(phone);
        this.email.set(email);
        this.setDob(dob);
        this.group.set(group);
    }


    public String getFirstName() {
        return firstName.get();
    }

    public SimpleStringProperty firstNameProperty() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getLastName() {
        return lastName.get();
    }

    public SimpleStringProperty lastNameProperty() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public String getPhone() {
        return phone.get();
    }

    public SimpleStringProperty phoneProperty() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public LocalDate getDob() {
        return dobProperty().get();
    }

    public SimpleObjectProperty<LocalDate> dobProperty() {
        if (dob == null) {
            dob = new SimpleObjectProperty<>();
        }
        return dob;
    }


    public void setDob(LocalDate dob) {
        dobProperty().set(dob);
    }

    public String getGroup() {
        return group.get();
    }

    public SimpleStringProperty groupProperty() {
        return group;
    }

    public void setGroup(String group) {
        this.group.set(group);
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Contact contact = (Contact) obj;
        return this.getFirstName().equals(contact.getFirstName())
                && this.getLastName().equals(contact.getLastName())
                && this.getPhone().equals(contact.getPhone())
                && this.getEmail().equals(contact.getEmail())
                && this.getDob().equals(contact.getDob())
                && this.getGroup().equals(contact.getGroup());
    }

    // It should return Id if the class is using SQL
    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

