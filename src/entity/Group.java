package entity;
import javafx.beans.property.SimpleStringProperty;


public class Group {
    private SimpleStringProperty name = new SimpleStringProperty("");

    public Group(String name) {
        this.name.set(name);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Group group = (Group) obj;
        return this.getName().equals(group.getName());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
