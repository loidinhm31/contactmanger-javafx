module ContactManager {
    requires javafx.fxml;
    requires javafx.controls;

    opens ui;
    opens controller;
    opens entity;
    opens dao;
}