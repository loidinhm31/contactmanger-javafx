package dao;


import entity.Group;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;


public class GroupDAO {
//    private static GroupDAO instance;
    private static final String filename = "group.txt";
    private final ObservableList<Group> groupObservableList;


    public GroupDAO() {
        groupObservableList = FXCollections.observableArrayList();
    }

//    public static GroupDAO getInstance() {
//        if (instance == null) {
//            instance = new GroupDAO();
//        }
//        return instance;
//    }

    public ObservableList<Group> getGroupItem() {
        return groupObservableList;
    }


    // Load all groups from the file group in to a list
    public void loadGroup() {

        Path locPath = FileSystems.getDefault().getPath("data" + File.separator + filename);

        if (Files.exists(locPath)) {
            String input;
            try(BufferedReader br = Files.newBufferedReader(locPath)) {
                while ((input = br.readLine()) != null) {
                    groupObservableList.add(new Group(input));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    // Save all groups from a given list to a text file
    public void saveGroupToFile()  {
        Path locPath = FileSystems.getDefault().getPath("data" + File.separator + filename);

        try (BufferedWriter bw = Files.newBufferedWriter(locPath)) {
            for (Group groupItem : groupObservableList) {
                bw.write(groupItem.getName());
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addGroupItem(Group item) {
        groupObservableList.add(item);
    }

    public void deleteGroupItem(Group group) {
        groupObservableList.remove(group);
    }

}
