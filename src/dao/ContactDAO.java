package dao;

import entity.Contact;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;


public class ContactDAO {
    private static ContactDAO instance;
    private static final String filename = "contact.txt";
    private ObservableList<Contact> contactObservableList;

    public DateTimeFormatter formatter;

    private ContactDAO() {
        contactObservableList = FXCollections.observableArrayList();
        formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    }

    public static ContactDAO getInstance() {
        if (instance == null) {
            instance = new ContactDAO();
        }
        return instance;
    }

    public ObservableList<Contact> getContact() {
        return contactObservableList;
    }


    // Save all Contacts from a given list to a text file
    public void saveToFile() {
        Path locPath = FileSystems.getDefault().getPath("data" + File.separator + filename);

        try (BufferedWriter bw = Files.newBufferedWriter(locPath)) {
            Iterator<Contact> iter = contactObservableList.iterator();
            while (iter.hasNext()) {
                Contact contactItem = iter.next();
                bw.write(String.format("%s:%s:%s:%s:%s:%s",
                        contactItem.getFirstName(),
                        contactItem.getLastName(),
                        contactItem.getPhone(),
                        contactItem.getEmail(),
                        contactItem.getDob().format(formatter),
                        contactItem.getGroup()));
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Add contact to current list
    public void addContactItem(Contact contact) {
        contactObservableList.add(contact);
    }

    // Load all Contacts from the file (file_name) in to an observable list
    public void loadContact() {
        Path locPath = FileSystems.getDefault().getPath("data" + File.separator + filename);
        if (Files.exists(locPath)) {
            String input;

            try (BufferedReader br = Files.newBufferedReader(locPath)) {
                while ((input = br.readLine()) != null) {
                    String[] contactPieces = input.split(":");

                    String firstName = contactPieces[0];
                    String lastName = contactPieces[1];
                    String phone = contactPieces[2];
                    String email = contactPieces[3];
                    LocalDate dob = LocalDate.parse(contactPieces[4], formatter);
                    String group = contactPieces[5];

                    Contact contact = new Contact(firstName, lastName, phone, email, dob, group);
                    contactObservableList.add(contact);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    // Remove contact from current list
    public void deleteContactItem(Contact contact) {
        contactObservableList.remove(contact);
    }
    
}
